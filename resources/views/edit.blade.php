@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    @foreach($admins as $admin)
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title">Вы собираетесь изменить пароль Администратора "{{ ucfirst($admin->name) }}
                                "</h4>
                        </div>
                        <div class="card-content">
                            <form method="post" action="{{ route('adminEditPass') }}">

                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label class="control-label">Пароль</label>
                                            <input id="password" type="password" class="form-control" name="password"
                                                   required>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" name="edit" value="{{ $admin->id }}"
                                        class="btn btn-primary pull-right">Изменить
                                </button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
