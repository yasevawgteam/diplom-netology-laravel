<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ asset('css/reset.css') }}"> <!-- CSS reset -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}"> <!-- Resource style -->
    <script src="{{ asset('js/modernizr.js') }}"></script> <!-- Modernizr -->
    <title>Тех-поддержка</title>
</head>
<body>
@if (Route::has('login'))
    <div class="top-right links">
        @if (Auth::check())
            <div style="padding: 5px">
                <a href="{{ url('/admin') }}">Админка</a>
            </div>

        @endif
    </div>
@endif
<header>
    <h1>Тех-поддержка =)</h1>

</header>
@if(Session::has('message'))
    <h4 class="title">{{Session::get('message')}}</h4>
@endif
@yield('content')

<script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
<script src="{{ asset('js/jquery.mobile.custom.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script> <!-- Resource jQuery -->

</body>
</html>