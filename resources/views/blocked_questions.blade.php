@extends('admin')

@section('admin_data')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title">Вопросы</h4>
                            <p class="category">Заблокированные</p>
                        </div>
                        @foreach($questions as $question)
                            <div class="card-content table-responsive">
                                <table class="table">
                                    <tr>
                                        <td>{{ ucfirst($question->question) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Дата создания: {{ ucfirst($question->created_at) }}</td>
                                        <td>
                                            Статус: {{ \App\Http\Controllers\AdminController::status($question->id) }}</td>
                                        <td>Автор: {{ ucfirst($question->name) }}</td>
                                        <td>
                                            Категория: {{ \App\Http\Controllers\AdminController::getQuestionCategory($question->categorys_id) }}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Причина блокировки: найдено слово - <span
                                                    style="color: red">{{ ucfirst($question->forbidden_words)}}</span>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="display: inline-flex">

                                            <form method="post"
                                                  action="{{ route('questionDel', ['id' => $question->id]) }}">
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger">Удалить
                                                </button>
                                            </form>

                                            <form method="post"
                                                  action="{{ route('viewEditQuestion', ['id' => $question->id]) }}">
                                                <button type="submit" class="btn btn-primary">Редактировать
                                                </button>
                                            </form>

                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>

                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection