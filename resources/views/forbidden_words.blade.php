@extends('admin')

@section('admin_data')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title">Список запрещенных слов</h4>
                            <p class="category">18+</p>
                        </div>
                        <div class="card-content table-responsive">
                            <table class="table">
                                <tr>
                                    @foreach($forbiddens as $forbidden)
                                        <td>{{ ucfirst($forbidden) }}</td>
                                    @endforeach
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="card">
                            <div class="card-header" data-background-color="purple">
                                <h4 class="title">Добавить слово</h4>
                            </div>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('addForbiddenWord') }}">

                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">Введите запрещенное слово:</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="word" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">Добавить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection