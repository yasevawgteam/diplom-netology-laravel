@extends('admin')

@section('admin_data')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title">Панель редактирования</h4>
                            <p class="category">Отредактируйте вопрос по своему усмотрению</p>
                        </div>
                        <div class="card-content table-responsive">
                            <form id="form" method="get" action="{{ route('adminEditQuestion') }}">
                                <table class="table">
                                    @foreach($questions as $question)
                                        <tr>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label">Вопрос</label>
                                                            <input type="text" value="{{ $question->question }}"
                                                                   class="form-control" name="question" required
                                                                   autofocus>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label">Автор</label>
                                                            <input type="text" value="{{ $question->name }}"
                                                                   class="form-control" name="name" required autofocus>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Статус</label>
                                                            <select name="public">
                                                                <option></option>
                                                                <option value="1">Скрыть</option>
                                                                <option value="0">Ожидает ответа</option>
                                                                <option value="2">Опубликовать</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Тема</label>
                                                            <select typeof="" name="categorys_id">
                                                                <option></option>
                                                                @foreach($categorys as $category)
                                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="submit" name="do_edit" value="{{ $question->id }}"
                                                        class="btn btn-primary pull-right">Изменить
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection