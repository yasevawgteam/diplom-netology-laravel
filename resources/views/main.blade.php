@extends('layouts.page')

@section('content')
    <div style="text-align: center; padding: 5px; font-size: 26px">
        <a href="{{ route('viewAsk') }}">Задать вопрос</a>
    </div>

    <section class="cd-faq">

        <ul class="cd-faq-categories">
            @foreach($categorys as $category)
                <li><a href="#{{ $category->name }}">{{ ucfirst($category->name) }}</a></li>
            @endforeach
        </ul> <!-- cd-faq-categories -->

        <div class="cd-faq-items">
            @foreach($categorys as $category)
                <ul id="{{ $category->name }}" class="cd-faq-group">
                    <li class="cd-faq-title"><h2>{{ ucfirst($category->name) }}</h2></li>
                    @foreach(\App\Http\Controllers\IndexController::viewAllQuestOneCategory($category->id) as $question)
                        <li>
                            @if($question->public > 1)
                                <a class="cd-faq-trigger" href="#0">{{ $question->question }}</a>
                                <div class="cd-faq-content">
                                    <p>{{ \App\Http\Controllers\IndexController::viewAnswer($question->id)['answer'] }}</p>
                                </div> <!-- cd-faq-content -->
                            @endif
                        </li>
                    @endforeach
                </ul> <!-- cd-faq-group -->
            @endforeach

        </div> <!-- cd-faq-items -->
        <a href="#0" class="cd-close-panel">Close</a>
    </section> <!-- cd-faq -->

@endsection