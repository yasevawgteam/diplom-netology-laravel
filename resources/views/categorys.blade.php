@extends('admin')

@section('admin_data')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title">Темы</h4>
                            <p class="category">Это таблица всех тем которые есть на сайте</p>
                        </div>
                        <div class="card-content table-responsive">
                            <table class="table">
                                <thead class="text-primary">
                                <th>Темы</th>
                                <th>Всего вопросов</th>
                                <th>Опубликовано</th>
                                <th>Без ответов</th>
                                <th></th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($categorys as $category)
                                <tr>
                                    <td>
                                        <a href="{{ route('adminViewQuestions', ['id' => $category->id]) }}">{{ ucfirst($category->name) }}</a>
                                    </td>
                                    <td>{{ \App\Http\Controllers\AdminController::countQuestions($category->id) }}</td>
                                    <td>{{ \App\Http\Controllers\AdminController::countPublicQuestions($category->id) }}</td>

                                    <td>{{ \App\Http\Controllers\AdminController::countNonAnswerQuestions($category->id) }}</td>

                                    <td style="display: inline-flex">
                                        <form method="post"
                                              action="{{ route('categoryDel') }}">
                                            {{ method_field('DELETE') }}
                                            <button type="submit" name="id" value="{{ $category->id }}"
                                                    class="btn btn-primary pull-right">Удалить
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title">Создать новую тему</h4>
                        </div>
                        <div class="card-content">
                            <form method="post" action="{{ route('adminAddCategory') }}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Введите название темы</label>
                                            <input id="name" type="text" class="form-control" name="name" required>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary pull-right">Добавить</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection