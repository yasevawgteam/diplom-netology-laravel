@extends('admin')

@section('admin_data')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header" data-background-color="purple">
                            @foreach($categorys as $category)
                                <h4 class="title">{{ ucfirst($category->name) }}</h4>
                            @endforeach

                            <p class="category">Вопросы</p>
                        </div>
                        @foreach($questions as $question)
                        <div class="card-content table-responsive">
                            <table class="table">
                                <tr>
                                    <td>{{ ucfirst($question->question) }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Ответ: {{ \App\Http\Controllers\IndexController::viewAnswer($question->id)['answer'] }}</td>
                                </tr>
                                <tr>
                                    <td>Дата создания: {{ ucfirst($question->created_at) }}</td>
                                    <td>Статус: {{ \App\Http\Controllers\AdminController::status($question->id) }}</td>
                                    <td>Автор: {{ ucfirst($question->name) }}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="display: inline-flex">
                                            <form method="post"
                                                  action="{{ route('questionDel') }}">
                                                {{ method_field('DELETE') }}
                                                <button type="submit" name="id" value="{{ $question->id }}"
                                                        class="btn btn-danger">Удалить
                                                </button>
                                            </form>

                                        @if($question->public !== 2)
                                            <form method="post"
                                                  action="{{ route('publicQuestion') }}">
                                                <input type="hidden" name="public" value="2">
                                                <button type="submit" name="id" value="{{ $question->id }}"
                                                        class="btn btn-success">Опубликовать
                                                </button>
                                            </form>

                                        @else

                                            <form method="post"
                                                  action="{{ route('publicQuestion') }}">
                                                <input type="hidden" name="public" value="1">
                                                <button type="submit" name="id" value="{{ $question->id }}"
                                                        class="btn btn-warning">Снять с публикации
                                                </button>
                                            </form>

                                        @endif

                                            <form method="post"
                                                  action="{{ route('viewEditQuestion', ['id' => $question->id]) }}">
                                                <button type="submit" class="btn btn-primary">Редактировать
                                                </button>
                                            </form>
                                        <form method="post"
                                              action="{{ route('adminAnswerQuestion', ['id' => $question->id]) }}">
                                            @if(\App\Http\Controllers\IndexController::viewAnswer($question->id)['answer'])
                                                <button type="submit" class="btn btn-success">Изменить ответ</button>
                                            @else
                                                <button type="submit" class="btn btn-primary">Ответить</button>
                                            @endif
                                        </form>
                                        </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                            <hr>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection