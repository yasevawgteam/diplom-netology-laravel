@extends('admin')

@section('admin_data')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="purple">

                            <h4 class="title">Форма ответа</h4>
                            <p class="category">Вопрос</p>
                        </div>
                        <div class="card-content table-responsive">
                            <form method="post" action="{{ route('saveAnswerQuestion') }}">
                                <table class="table">
                                    <tbody>
                                    @foreach($questions as $question)
                                        <tr>
                                            <td>{{ ucfirst($question->question) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Дата создания: {{ $question->created_at }}</td>
                                            <td>
                                                Статус: {{ \App\Http\Controllers\AdminController::status($question->id) }}</td>
                                            <td>Автор: {{ ucfirst($question->name) }}</td>
                                        </tr>
                                        <tr>

                                        <tr>

                                    </tbody>
                                </table>

                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Ваш ответ</label>
                                                <input type="text"
                                                       value="{{ \App\Http\Controllers\IndexController::viewAnswer($question->id)['answer'] }}"
                                                       class="form-control" name="answer" required
                                                       autofocus>
                                            </div>
                                        </div>
                                    </div>
                                <td>
                                </td>
                                @if(\App\Http\Controllers\IndexController::viewAnswer($question->id)['answer'])
                                    <button type="submit" name="questions_id"
                                            value="{{ \App\Http\Controllers\IndexController::viewAnswer($question->id)['id'] }}"
                                            class="btn btn-primary pull-right">Ответить
                                    </button>
                                @else
                                    <button type="submit" name="questions_id" value="{{ $question->id }}"
                                            class="btn btn-primary pull-right">Ответить
                                    </button>
                                @endif
                                @endforeach
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection