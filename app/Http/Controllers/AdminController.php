<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Category;
use App\ForbiddenWords;
use App\Http\Requests\ValidCategory;
use App\Question;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Telegram\Bot\Api;

class AdminController extends Controller
{
    /**
     * Show all categories
     */
    public function viewCategorys()
    {
        $categorys = Category::select(['id', 'name', 'created_at'])->get();
        $questions = Question::select(['id'])->get();
        return view('categorys')->with(['categorys' => $categorys, 'questions' => $questions]);
    }

    /**
     * Show all questions without answers
     */
    public function showNonAnswerQuestions()
    {
        $questions = Question::where('public', '>=', 0)->get();
        $allNonAnsw = array();
        foreach ($questions as $question) {
            $answer = Question::find($question['id'])->answer;
            if (!$answer) {
                $nonAnswQuests = Question::where('id', '=', $question['id'])->orderBy('id')->get();
                foreach ($nonAnswQuests as $nonAnswQuest) {
                    $allNonAnsw[] = $nonAnswQuest;
                }
            }
        }
        return view('all_questions')->with(['questions' => $questions, 'nonanswerarray' => $allNonAnsw]);
    }

    /**
     * Show quantity questions without answer in one category
     * @param int $id Category id
     */
    public static function countNonAnswerQuestions($categorys_id)
    {
        $questions = new Question();
        $answQuestion = $questions->countAnswerQuestionsInCategory($categorys_id);
        $allQuestions = $questions->countAllQuestionInCategory($categorys_id);
        return $allQuestions - $answQuestion;
    }

    /**
     * Receives the category to which the question relates
     * @param int $id Category id
     */
    public static function getQuestionCategory($categorys_id)
    {
        $categorys = Category::find($categorys_id);
        if ($categorys) {
            return ucfirst($categorys->name);
        }

    }

    /**
     * Show quantity all questions in one category
     * @param int $id Category id
     */
    public static function countQuestions($id)
    {
        $question = Question::where('categorys_id', '=', $id);
        return $question->count();
    }

    /**
     * Show quantity published questions in one category
     * @param int $id Category id
     */
    public static function countPublicQuestions($id)
    {
        $question = Question::whereRaw('public > 1 and categorys_id = ?', [$id]);
        return $question->count();
    }

    /**
     * Show page of administrators
     */
    public function viewAdmins()
    {
        $admins = User::select(['id', 'name', 'email', 'created_at'])->get();
        return view('admins')->with(['admins' => $admins]);
    }

    /**
     * Create new administrator
     * @param array $request An array of form data
     */
    public function createAdmin(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:5'
        ]);
        $admin = new User();
        $admin->create($data);
        self::logging('Добавил пользователя', $data['name']);
        return redirect('/admin/admins');
    }

    /**
     * Delete administrator
     * @param array $request Admin id
     */
    public function deleteAdmin(User $id)
    {
        $id->delete();
        self::logging('Удалил пользователя', $id);
        return redirect('/admin/admins');
    }

    /**
     * Show page for change administrator password
     * @param array $request Admin id
     */
    public function viewEdit(Request $request)
    {
        $data = $request->all();
        $admins = User::findOrFail(['id' => $data['id']]);
        return view('edit', ['admins' => $admins]);
    }

    /**
     * Change and save administrator password
     * @param array $request An array of form data, new password for admin
     */
    public function editPass(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'password' => 'required|min:5'
        ]);
        $admins = User::findOrFail($data['edit']);
        $data['password'] = bcrypt($data['password']);
        $admins->password = $data['password'];
        $admins->save();
        self::logging('Обновил пароль пользователя', $data['edit']);
        return redirect('admin/admins')->with(['message' => 'Данные сохранены']);
    }

    /**
     * Create new category
     * @param array $request An array of form data
     */
    public function addCategory(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required|max:55'
        ]);
        $category = new Category();
        $category->fill($data);
        $category->save();
        self::logging('Добавил категорию', $data['name']);
        return redirect('/admin/categorys')->with(['message' => 'Данные сохранены']);
    }

    /**
     * Delete category and all questions and answers related to it
     * @param array $request Category id
     */
    public function deleteCategory(Request $request)
    {
        $data = $request->all();
        $questions = Question::where('categorys_id', '=', $data['id']);
        foreach ($questions->get() as $question) {
            $this->deleteAnswer($question->id);
        }
        if ($questions) {
            $questions->delete();
        }
        $category = Category::find($data['id']);
        $category->delete();
        self::logging('Удалил категорию', '"' . $category->name . '" ' . 'Все вопросы и ответы относящееся к ней');
        return redirect('admin/categorys')->with(['message' => 'Данные удалены']);
    }

    /**
     * Delete one question and answer related to it
     * @param array $request Question id
     */
    public function deleteQuestion(Request $request)
    {
        $data = $request->all();
        $questions = Question::where('id', '=', $data['id']);
        $this->deleteAnswer($data['id']);
        $questions->delete();
        return redirect()->back()->with(['message' => 'Данные удалены']);
    }

    /**
     * Delete answer to question
     * @param int $id Question id
     * @return bool
     */
    public function deleteAnswer($id)
    {
        $deleteAnswer = Answer::where('questions_id', '=', $id);
        if ($deleteAnswer) {
            $deleteAnswer->delete();
        }
        return true;
    }

    /**
     * Show page of questions of the same category
     * @param int $id Category id
     */
    public function viewQuestions($id)
    {
        $categorys = Category::where('id', '=', $id)->get();
        $questions = Question::where('categorys_id', '=', $id)->get();
        return view('questions')->with(['categorys' => $categorys, 'questions' => $questions]);
    }

    /**
     * Shows the status of the question
     * @param int $id Question id
     */
    public static function status($id)
    {
        $questions = new Question();
        return $questions->status($id);
    }

    /**
     * Shows the page for editing the question
     * @param int $id Question id
     */
    public function viewEditQuestion($id)
    {
        $questions = Question::where('id', '=', $id)->get();
        $categorys = Category::select(['id', 'name'])->get();
        return view('edit_question')->with(['categorys' => $categorys, 'questions' => $questions]);
    }

    /**
     * Method publishes a question and removes it from a publication
     * @param array $request Question id
     */
    public function publicQuestion(Request $request)
    {
        $data = $request->all();
        $question = Question::findOrFail($data['id']);
        $question->public = $data['public'];
        if ($data['public'] == 2) {
            self::logging('Опубликовал вопрос', $data['id']);
        } else {
            self::logging('Снял с публикации вопрос', $data['id']);
        }
        $question->save();
        return redirect()->back()->with(['message' => 'Обновлено']);
    }

    /**
     * Change and save the same question
     * @param array $request An array of form data(question, author, category and status)
     */
    public function editQuestion(Request $request)
    {
        $data = $request->all();
        $question = Question::find($data['do_edit']);

        if (isset($question->name)) {
            $question->name = $data['name'];
        }
        if (isset($question->question)) {
            $question->question = $data['question'];
        }
        if (isset($data['public'])) {
            $question->public = $data['public'];
        }
        if (isset($data['categorys_id'])) {
            $question->categorys_id = $data['categorys_id'];
        }
        self::logging('Обновил вопрос', $data['do_edit']);
        $question->save();
        return redirect('admin/categorys')->with(['message' => 'Данные сохранены']);
    }

    /**
     * Single question display
     * @param int $id Question id
     */
    public function viewOneQuestion($id)
    {
        $questions = Question::where('id', '=', $id)->get();
        return view('answer')->with(['questions' => $questions]);
    }

    /**
     * Save answer for the question
     * @param array $request An array of form data
     */
    public function saveAnswer(Request $request)
    {
        $data = $request->all();
        $answer = Answer::find($data['questions_id']);
        if (isset($answer)) {
            $answer->answer = $data['answer'];
            self::logging('Обновил ответ на вопрос', $data['questions_id']);
            $answer->save();
        } else {
            $answer = new Answer();
            $this->validate($request, [
                'answer' => 'required'
            ]);
            $answer->fill($data);
            self::logging('Ответил на вопрос', $data['questions_id']);
            $answer->save();
        }
        return redirect('/admin/categorys')->with(['message' => 'Данные сохранены']);
    }

    /**
     * Show answer for the same question
     * @param int $id Question id
     */
    public static function showAnswer($id)
    {
        $answers = Answer::where('questions_id', '=', $id)->get();
        return $answers;
    }

    /**
     * Search questions
     * @param array $request An array of form data(search data)
     */
    public function search(Request $request)
    {
        $questions = Question::where('question', 'like', '%' . $request->get('symb') . '%')->get();
        return view('admin_search', ['questions' => $questions]);
    }

    /**
     * Logging of actions of administrators
     */
    public function logging($info, $id = '')
    {
        Log::info(Auth::user()->name . ' ' . $info . ' (' . $id . ')');
    }

    /**
     * Show forbidden questions
     */
    public function viewForbiddenQuestions()
    {
        $questions = Question::where('public', '=', "-1")->get();
        return view('blocked_questions', ['questions' => $questions]);
    }

    /**
     * Show forbidden words
     */
    public function viewForbiddenWords()
    {
        $forbiddenWords = IndexController::forbiddenWords();
        return view('forbidden_words', ['forbiddens' => $forbiddenWords]);
    }

    /**
     * Create and save forbidden words
     * @param array $request An array of form data(new forbidden word)
     */
    public function addForbiddenWord(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'forbidden_words' => 'required'
        ]);
        $word = new ForbiddenWords();
        $word->fill($data);
        $word->save();
        return redirect()->back();
    }
    /*
        public function telegramm ()
        {
            $telegramm = new Api(env('TELEGRAM_BOT_TOKEN';

            $response = $telegramm->sendMessage([
                'chat_id' => '340378150',
                'text' => 'Кукареку'
            ]);


        $response = $telegramm->getUpdates();

       // $messageId = $response->getMessageId();
        dd($response);
        return view('telegramm', ['response' => $response ]);
    }
    */
}
