<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegrammController extends Controller
{
    public function getHome()
    {
        return view('telegramm');
    }

    public function getUpdates()
    {
        $updates = Telegram::getUpdates();
        $questArray = Question::select(['message_id'])->get();

        $dbArray = array();
        foreach ($questArray as $item) {
            $dbArray[] = $item['message_id'];
        }

        $checkMessage = new IndexController();

        $data = array();
        for ($i = 0; $i < count($updates); $i++) {
            $data['message_id'] = $updates[$i]['message']['message_id'];
            $data['email'] = $updates[$i]['message']['from']['id'];
            $data['name'] = $updates[$i]['message']['from']['first_name'];
            $data['question'] = $updates[$i]['message']['text'];
            if (!in_array($data['message_id'], $dbArray)) {
                $forbiddenWord = $checkMessage->controlTextQuestion($data['question'], IndexController::forbiddenWords());
                $questions = new Question();
                if (count($forbiddenWord) !== 0) {
                    $strForbidden = implode(", ", $forbiddenWord);
                    $data['forbidden_words'] = $strForbidden;
                    $data['public'] = "-1";
                }
                $questions->fill($data);
                $questions->save();
            }
        }
        return redirect('/');
    }

    public function getSendMessage()
    {
        return view('send-message');
    }

    public function postSendMessage(Request $request)
    {
        $rules = [
            'message' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                ->with('status', 'danger')
                ->with('message', 'Message is required');
        }
        Telegram::sendMessage([
            'chat_id' => 340378150,
            'text' => $request->get('message')
        ]);
        return redirect()->back()
            ->with('status', 'success')
            ->with('message', 'Message sent');
    }
}