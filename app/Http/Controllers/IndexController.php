<?php

namespace App\Http\Controllers;

use App\ForbiddenWords;
use App\Question;
use App\Category;
use App\Http\Requests\ValidAskForm;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Show main page
     */
    public function viewMain()
    {
        $categorys = Category::select(['id', 'name'])->get();
        return view('main')->with(['categorys' => $categorys]);
    }

    /**
     * Show all questions in one category
     * @param int $id Category id
     */
    public static function viewAllQuestOneCategory($id)
    {
        $questions = Question::where('categorys_id', '=', $id)->get();
        return $questions;
    }

    /**
     * Show answer of question
     * @param int $id Answer id
     */
    public static function viewAnswer($id)
    {
        $answer = Question::find($id)->answer;
        if ($answer) {
            return $answer;
        }
    }

    /**
     * Show page for ask questions
     */
    public function viewAsk()
    {
        $categorys = Category::select(['id', 'name'])->get();
        return view('ask')->with(['categorys' => $categorys]);
    }

    /**
     * Create, check and save question
     * @param array $request An array of form data(name, email, question)
     */
    public function setAsk(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'question' => 'required'
        ]);
        $forbiddenWord = $this->controlTextQuestion($data['question'], self::forbiddenWords());
        $question = new Question();
        if (count($forbiddenWord) !== 0) {
            $strForbidden = implode(", ", $forbiddenWord);
            $data['forbidden_words'] = $strForbidden;
            $data['public'] = "-1";
        }
        $question->fill($data);
        $question->save();
        return redirect('/')->with(['message' => 'Ваш вопрос отправлен на модерацию']);
    }

    /**
     * Checking text for banned words
     * @param string $text Question text
     * @param array $words Array banned words
     * @return array
     */
    public function controlTextQuestion($text, $words)
    {
        $findArray = array();
        foreach ($words as $item) {
            if (preg_match("/$item/", $text)) {
                $findArray[] = $item;
            }
        }
        return $findArray;
    }

    /**
     * Get a list of forbidden words from the database
     * @return array
     */
    public static function forbiddenWords()
    {
        $allWords = array();
        $dbWords = ForbiddenWords::select('word')->get();
        foreach ($dbWords as $word) {
            $allWords[] = $word['word'];
        }
        return $allWords;
    }
}
