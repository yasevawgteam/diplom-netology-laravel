<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allQuestions = Question::where('public', '=', 0);
        $questions = $allQuestions->get();
        $countNewQuestions = $allQuestions->count();
        return view('admin')->with(['questions' => $questions]);
    }
}
