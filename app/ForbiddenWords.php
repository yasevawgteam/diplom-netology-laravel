<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForbiddenWords extends Model
{
    protected $fillable = ['word'];
    protected $table = 'forbidden_words';
}
