<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Question extends Model
{
    protected $fillable = ['categorys_id', 'name', 'email', 'question', 'public', 'forbidden_words', 'message_id'];

    protected $table = 'questions';

    /**
     * Answer of question
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function answer()
    {
        return $this->hasOne('App\Answer', 'questions_id');
    }

    /**
     * Status of questions
     */
    public function status($id)
    {
        $questions = self::where('id', '=', $id)->get();
        foreach ($questions as $question) {
            if ($question['public'] === 1) {
                $status = 'Скрыто';
                return $status;
            } elseif ($question['public'] === 0) {
                $status = 'Ожидает ответа';
                return $status;
            } elseif ($question['public'] < 0) {
                $status = 'Заблокирован';
                return $status;
            } else {
                $status = 'Опубликовано';
                return $status;
            }
        }
    }

    /**
     * Number of questions answered in one category
     */
    public function countAnswerQuestionsInCategory($id)
    {
        $questions = DB::table('questions')
            ->join('answers', 'questions.id', '=', 'answers.questions_id')
            ->where('questions.categorys_id', '=', $id)
            ->count();
        return $questions;
    }

    /**
     * Quantity all questions in one category
     */
    public function countAllQuestionInCategory($id)
    {
        $questions = DB::table('questions')
            ->where('categorys_id', '=', $id)
            ->count();
        return $questions;
    }
}
