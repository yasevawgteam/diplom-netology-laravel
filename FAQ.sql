�������� ���� ������.
��������� ��������� � ������� SQL

������� USERS
��������������. ������������ ������� ����� ������ � ����� ������ �����.
CREATE TABLE `users` (
	`id` INT(11) NOT NULL,
	`email` varchar(255) NOT NULL UNIQUE,
	`password` TEXT(11) NOT NULL,
	`remember_token` varchar(100),
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);

������� CATEGORYS
���� ���������� �������� �� �����.
CREATE TABLE `categorys` (
	`id` INT(11) NOT NULL,
	`name` varchar(100) NOT NULL UNIQUE,
	PRIMARY KEY (`id`)
);

������� QUESTIONS
�������� ��� ������� �����, id ���������, ����� � email ������������� ������� �� ������. 
������ ������� �� ����� � ������� ����������� ���� � �������.
CREATE TABLE `questions` (
	`id` INT NOT NULL,
	`categorys_id` INT(11) NOT NULL,
	`name` varchar(50) NOT NULL,
	`email` varchar(100),
	`question` TEXT(1000) NOT NULL,
	`forbidden_words` TEXT,
	`public` INT(1) DEFAULT '0',
	PRIMARY KEY (`id`)
);

������� ANSWERS
��� ������ � id �������� � �������� ��� ��������.
CREATE TABLE `answers` (
	`id` INT NOT NULL,
	`questions_id` INT NOT NULL,
	`answer` TEXT(1000) NOT NULL,
	PRIMARY KEY (`id`)
);

������� FORBIDDEN WORDS
������ ����������� ��� ������������� �� ����� ����.
CREATE TABLE `forbidden_words` (
	`id` INT NOT NULL,
	`word` varchar(55) NOT NULL,
	PRIMARY KEY (`id`)
);


