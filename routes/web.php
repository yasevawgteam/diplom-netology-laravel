<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@viewMain');

/**
 * Authorization
 */
Route::get('auth/register', 'Auth\AuthController@getRegister');
Auth::routes();
Route::get('/admin', 'HomeController@index');

// telegramm
Route::get('/telegramm', ['middleware' => 'auth', 'uses' => 'TelegrammController@getHome']);
Route::get('/get-updates', ['middleware' => 'auth', 'uses' => 'TelegrammController@getUpdates']);
Route::get('/send', ['middleware' => 'auth', 'uses' => 'TelegrammController@getSendMessage']);
Route::post('/send', ['middleware' => 'auth', 'uses' => 'TelegrammController@postSendMessage']);

/**
 * Ask questions
 */
Route::get('ask', 'IndexController@viewAsk')->name('viewAsk');
Route::post('ask', 'IndexController@setAsk')->name('indexSetAsk');

/**
 * Admin panel
 */
Route::get('admin/admins', ['middleware' => 'auth', 'uses' => 'AdminController@viewAdmins'])->name('adminViewAdmins');
Route::post('admin/admins', ['middleware' => 'auth', 'uses' => 'AdminController@createAdmin'])->name('adminAddAdmin');

// delete admin
Route::delete('delete/{id}', ['middleware' => 'auth', 'uses' => 'AdminController@deleteAdmin'])->name('adminDel');

// edit password
Route::post('edit', ['middleware' => 'auth', 'uses' => 'AdminController@viewEdit'])->name('adminViewEdit');
Route::post('save/{edit?}', ['middleware' => 'auth', 'uses' => 'AdminController@editPass'])->name('adminEditPass');

// category
Route::get('admin/categorys', ['middleware' => 'auth', 'uses' => 'AdminController@viewCategorys'])->name('adminViewCategorys');
Route::post('admin/categorys', ['middleware' => 'auth', 'uses' => 'AdminController@addCategory'])->name('adminAddCategory');
Route::delete('categorys/delete', ['middleware' => 'auth', 'uses' => 'AdminController@deleteCategory'])->name('categoryDel');

// questions
Route::get('admin/categorys/{id}', ['middleware' => 'auth', 'uses' => 'AdminController@viewQuestions'])->name('adminViewQuestions');
Route::delete('question/delete/{id?}', ['middleware' => 'auth', 'uses' => 'AdminController@deleteQuestion'])->name('questionDel');
Route::post('question/{id}', ['middleware' => 'auth', 'uses' => 'AdminController@viewEditQuestion'])->name('viewEditQuestion');
Route::get('submit/{do_edit?}', ['middleware' => 'auth', 'uses' => 'AdminController@editQuestion'])->name('adminEditQuestion');
Route::post('public/{public?}', ['middleware' => 'auth', 'uses' => 'AdminController@publicQuestion'])->name('publicQuestion');
Route::get('allquestion', ['middleware' => 'auth', 'uses' => 'AdminController@showNonAnswerQuestions'])->name('showAllQuestions');
Route::get('blocked_question', ['middleware' => 'auth', 'uses' => 'AdminController@viewForbiddenQuestions'])->name('showBlockedQuestions');

// answers
Route::post('question/answer/{id}', ['middleware' => 'auth', 'uses' => 'AdminController@viewOneQuestion'])->name('adminAnswerQuestion');
Route::post('answer/save/{id?}', ['middleware' => 'auth', 'uses' => 'AdminController@saveAnswer'])->name('saveAnswerQuestion');

// search
Route::get('search', ['middleware' => 'auth', 'uses' => 'AdminController@search'])->name('adminSearch');

// forbidden words
Route::get('forbidden', ['middleware' => 'auth', 'uses' => 'AdminController@viewForbiddenWords'])->name('viewForbiddenWords');
Route::post('addforbidden', ['middleware' => 'auth', 'uses' => 'AdminController@addForbiddenWord'])->name('addForbiddenWord');
